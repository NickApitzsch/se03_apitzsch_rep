import java.util.Scanner;
public class Temperatur {

	public static void main(String[] args) {
		
		System.out.println("Bitte geben Sie den Startwert in Celsius an");
		Scanner myScanner = new Scanner(System.in);
		int startwert = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie den Endwert in Celsius an");
		Scanner input2 = new Scanner(System.in);
		int endwert = input2.nextInt();
		
		System.out.println("Bitte geben Sie den Schrittwert an");
		Scanner input3 = new Scanner(System.in);
		int schrittwert = input3.nextInt();		
		
		int zaehler = startwert;
		
		while(zaehler <= endwert) {
			double fahrenheit = 32 + (zaehler * 1.8);
			System.out.println(zaehler + "�C und " + fahrenheit + " �F");
			zaehler = zaehler + schrittwert;
		}
			

	}

}
