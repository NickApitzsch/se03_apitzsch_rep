import java.util.Scanner;


public class Taschenrechner {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double zahl1, zahl2, ergebnis;
		char operator, antwort;
		do {
			System.out.println("Bitte geben sie die erste Zahl ein");
			zahl1 = myScanner.nextDouble();
			
			System.out.println("Bitte geben sie den Operator an");
			operator = myScanner.next().charAt(0);
			
			System.out.println("Bitte geben sie die zweite Zahl ein");
			zahl2 = myScanner.nextDouble();
			
			if(operator == '+')
			{
				ergebnis = zahl1 + zahl2;
				System.out.println("Das Ergebnis ist: " + ergebnis);
			}
			if(operator == '-')
			{
				ergebnis = zahl1 - zahl2;
				System.out.println("Das Ergebnis ist: " + ergebnis);
			}
			if(operator == '*')
			{
				System.out.println("Das Ergebnis ist: " + (zahl1 * zahl2));
			}
			if(operator == '/')
			{
				System.out.println("Das Ergebnis ist " + (zahl1 / zahl2));
			}
			if(operator != '+' && operator != '-' && operator != '*' && operator != '/')
				System.out.println("Falscher Operator");
			
			System.out.println("Dr�cken Sie j um eine neue Berechnung zu starten oder eine beliebige Taste f�r Programmende");
		antwort = myScanner.next().charAt(0);
		}while (antwort == 'j');
			
		
	}
}