import java.util.Arrays;
import java.util.Scanner;

public class Array {

	public static void main(String[] args) {
		// zahlen();
		int[] zahlenkette = { 1, 2, 3, 4, 5};
        convertArrayToString(zahlenkette);
        System.out.println(convertArrayToString(zahlenkette));
		// ungeradeZahlen();
		// Buchstaben();
		
	}

	public static void zahlen() {

		int[] zahlenkette = new int[10];

		for (int i = 0; i < zahlenkette.length; i++) {
			zahlenkette[i] = i;
			System.out.print(zahlenkette[i] + " ");
		}
	}

	public static void ungeradeZahlen() {

		int[] zahlenkette = new int[10];

		for (int i = 0; i < zahlenkette.length; i++) {
			zahlenkette[i] = 2 * i + 1;
			System.out.print(zahlenkette[i] + " ");
		}
	}

	public static void Buchstaben() {

		Scanner myScanner = new Scanner(System.in);
		char[] buchstabenkette = new char[5];

		for (int i = 0; i < buchstabenkette.length; i++) {
			int a = i + 1;
			System.out.println("Bitte geben Sie das " + a + ". Zeichen ein");
			buchstabenkette[i] = myScanner.next().charAt(0);
			System.out.println(buchstabenkette[i]);
		}
		for (int i = buchstabenkette.length - 1; i >= 0; i--) {
			System.out.print(buchstabenkette[i]);
		}
		myScanner.close();
	}

	public static String convertArrayToString(int[] zahlenkette) {
		String zeichenkette = "[ ";

		for (int i = 0; i < zahlenkette.length; i++) {
			zeichenkette = zeichenkette + zahlenkette[i] + " ";
		
		}
		zeichenkette = zeichenkette + "]";
	        return zeichenkette;
	}
}
