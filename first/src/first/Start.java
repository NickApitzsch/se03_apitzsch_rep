package first;

public class Start {

	public static void main(String[] args) {
		// programmiersprache braucht Varibalen
		// Jede Variable hat einen Datentyp und einen Namen
		// in der Variable gibt es Werte

		// String ausgabe; // deklarieren
		// ausgabe = "Halloooo"; // initialisieren
		// oder azf einmal:
		//String ausgabe2 = "Haalihallo";
		// Variablennamen
		// 1. Zeichen = [a-z][A-Z] [_]
		// ab 2. Zeichen zusätzlich [0-9]
		// Verboten sind Schlüsseltwörter. z.B. String, if, class

		int zahl1 = 30112020;
		int zahl2 = 10000;
		int zahl3 = 44;
		int zahl4 = 12;
		double zahl5 = 2.0;
		double zahl6 = 3.0;
		// die Methode "+" nennt man "überladen"
		// Rechenarten + - * / %

		// System.out.println("Die Summe von " + zahl1 + " und " + zahl2 + " = " +
		// (zahl1 + zahl2));
		// integer wird zu double "gecastet"
		// quotient = (double) zahl1 / (double) zahl2;
		// System.out.println("Der Quotient " + zahl1 + " und " + zahl2 + " = " +
		// (quotient));

		// quotient = (double) zahl3 / (double) zahl4;
		// System.out.println("Der Quotient " + zahl3 + " und " + zahl4 + " = " +
		// (quotient));
		
	/*	
		Rechner r1 = new Rechner();
		r1.a = zahl1;
		r1.b = zahl2;
		r1.gibSummeAus();
		r1.gibDifferenzAus();
		r1.gibModuloAus();
		r1.gibProduktAus();
		r1.gibQuotientAus();
	*/
		//Rechner r2 = new Rechner();
		//r2.a = zahl3;
		//r2.b = zahl4;
		//r2.gibSummeAus();
		Rechner2 r2 = new Rechner2();
		r2.gibSummeAus(2, 3);
		r2.gibSummeAus(2.2, 3.9);
		r2.gibDifferenzAus(2, 3);
		r2.gibDifferenzAus(2.2, 3.9);
		r2.gibProduktAus(2, 3);
		r2.gibProduktAus(2.2, 3.9);
		r2.gibQuotientAus(2, 3);
		r2.gibQuotientAus(2.2, 3.9);
		r2.gibModuloAus(2, 3);
		r2.gibModuloAus(2.2, 3.9);
		System.exit(0);
	}


}
