package first;
/*
* Rechner2 soll int und double gleichzeitig 
*/
public class Rechner2 {
	
		
		public void gibSummeAus(int a, int b) {
			System.out.println("Die Summe von a " + a + " und " + b + " = " + (a + b));
		}
		//Die Methode gibSummeAus wird hier "�berladen". Java wei� aufgrund der �berrgebenen Paramer, welche Methode er nehmen muss.
		public void gibSummeAus(double a, double b) {
			System.out.println("Die Summe von a " + a + " und " + b + " = " + (a + b));
		}
		public void gibDifferenzAus(int a, int b) {
			System.out.println("Die Differenz von " + a + " und " + b + " = " + (a - b));
		}
		public void gibDifferenzAus(double a, double b) {
			System.out.println("Die Differenz von " + a + " und " + b + " = " + (a - b));
		}
		public void gibProduktAus(int a, int b) {
			System.out.println("Die Produkt von " + a + " und " + b + " = " + (a * b));
		}
		public void gibProduktAus(double a, double b) {
			System.out.println("Die Produkt von " + a + " und " + b + " = " + (a * b));
		}
		public void gibQuotientAus(int a, int b) {
			double quotient = (double) a / (double) b;
			System.out.println("Die Quotient von " + a + " und " + b + " = " + quotient);
		}
		public void gibQuotientAus(double a, double b) {
			double quotient = (double) a / (double) b;
			System.out.println("Die Quotient von " + a + " und " + b + " = " + quotient);
		}
		public void gibModuloAus(int a, int b) {
			System.out.println(a + " modulo " + b + " = " + (a % b));
		}
		public void gibModuloAus(double a, double b) {
			System.out.println(a + " modulo " + b + " = " + (a % b));
}
}


