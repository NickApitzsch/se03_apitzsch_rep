package first;

public class Rechner {
	// Attribute
	public String telNrRolf = "0176 34123350";
	public double a;
	public double b;
	
	// Methoden
	public void gibSummeAus() {
		System.out.println("Die Summe von " + a + " und " + b + " = " + (a + b));
	}
	public void gibDifferenzAus() {
		System.out.println("Die Differenz von " + a + " und " + b + " = " + (a - b));
	}
	public void gibProduktAus() {
		System.out.println("Die Produkt von " + a + " und " + b + " = " + (a * b));
	}
	public void gibQuotientAus() {
		// integer wird zu double "gecastet"
		double quotient = (double) a / (double) b;
		System.out.println("Die Quotient von " + a + " und " + b + " = " + quotient);
	}
	public void gibModuloAus() {
		System.out.println(a + " modulo " + b + " = " + (a % b));
	}
}
