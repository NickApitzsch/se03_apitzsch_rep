import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		double ticketPreis = 0;
		int anzahlTickets;
		int ticketAuswahl = 0;
		Scanner tastatur = new Scanner(System.in);

		System.out.print(
				"Waehlen Sie ihre Wunschfahrkarte f�r Berlin AB aus \n Einzelfahrschein Regeltarif AB [2,90 EUR] (1) \n Tageskarte Regeltarif AB [8,60 EUR] (2) \n Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
		while (ticketAuswahl <= 0 || ticketAuswahl >= 4) {
			ticketAuswahl = tastatur.nextInt();
			if (ticketAuswahl > 0 && ticketAuswahl < 4) {
				System.out.println("Ihre Wahl: " + ticketAuswahl);
			} else {
				System.out.println("Ihre Wahl " + ticketAuswahl + "\n Falsche Eingabe");
			}
		}
		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();
		if (ticketAuswahl == 1) {
			ticketPreis = 2.90;
		} else if (ticketAuswahl == 2) {
			ticketPreis = 8.60;
		} else if (ticketAuswahl == 3) {
			ticketPreis = 23.50;
		}
		return ticketPreis * anzahlTickets;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlender Betrag: %.2f Euro %n\"",
					(zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der Rueckgabebetrag in H�he von %4.2f Euro %n\"", rueckgabebetrag);
			System.out.println("wird in folgenden Muenzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-Muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-Muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Muezen
			{
				System.out.println("10 CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	public static void warte(int millisekunde) throws InterruptedException {
		Thread.sleep(millisekunde);
	}

	public static void main(String[] args) throws InterruptedException {
		while (true) {
			double zuZahlenderBetrag;
			double rueckgabebetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wuenschen Ihnen eine gute Fahrt.\n\n");
			warte(2000);
		}
	}

}