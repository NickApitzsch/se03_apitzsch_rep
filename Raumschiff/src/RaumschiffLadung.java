
public class RaumschiffLadung {

	private String bezeichnung;
	private int menge;

	public RaumschiffLadung() {
		this.bezeichnung = "Unbekannt";
		this.menge = 0;
	}

	public RaumschiffLadung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	public String getBezeichnung() {
		return this.bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return this.menge;
	}

	public void setMenge(int Menge) {
		this.menge = menge;
	}

	public String toString() {
		return "Ladung: " + this.bezeichnung + " , Menge: " + this.menge;
	}
}
