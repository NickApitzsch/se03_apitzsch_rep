import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<RaumschiffLadung> ladungsverzeichnis;

	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.schiffsname = "Unbekannt";
		this.androidenAnzahl = 0;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<RaumschiffLadung>();

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<RaumschiffLadung>();
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(RaumschiffLadung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);

	}

	public void photonentorpedoAbfeuern(Raumschiff ziel) {

		if (this.photonentorpedoAnzahl < 1) {
			broadcastKommunikator("-=*Click*=-");
		} else {
			this.photonentorpedoAnzahl--;
			broadcastKommunikator("Photonentorpedo abgeschossen");
			treffer(ziel);
		}
	}

	public void phaserkanoneAbfeuern(Raumschiff ziel) {
		if (this.energieversorgungInProzent < 50) {
			broadcastKommunikator("-=*Click*=-");
		} else {
			this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
			broadcastKommunikator("Phaserkanone abgeschossen");
			treffer(ziel);
		}
	}

	private void treffer(Raumschiff ziel) {
		System.out.println(ziel.schiffsname + "wurde getroffen!");
		ziel.schildeInProzent = ziel.schildeInProzent - 50;
		if (ziel.schildeInProzent <= 0) {
			ziel.huelleInProzent = ziel.huelleInProzent - 50;
			ziel.energieversorgungInProzent = ziel.energieversorgungInProzent - 50;
		}
		if (ziel.huelleInProzent <= 0) {
			ziel.lebenserhaltungssystemeInProzent = 0;
			broadcastKommunikator("Lebenserhaltungssysteme wurden vernichtet!");
		}
	}

	public void broadcastKommunikator(String message) {
		broadcastKommunikator.add(message);
		System.out.println(message);
	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;
	}

	public void photonentorpedoLaden(int anzahlTorpedos) {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			RaumschiffLadung ladung = ladungsverzeichnis.get(i);
			if (ladung.getBezeichnung().equals("Photonentorpedo")) {
				if (anzahlTorpedos > ladung.getMenge()) {
					this.photonentorpedoAnzahl = ladung.getMenge();
					System.out.println(ladung.getMenge() + "Photonentorpedo eingesetzt");
					ladung.setMenge(0);
				} else {
					ladung.setMenge(ladung.getMenge() - anzahlTorpedos);
					this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
					System.out.println(anzahlTorpedos + "Photonentorpedo eingesetzt");
				}
			}
		}
		System.out.println("Keine Photonentorpedos gefunden");
		this.broadcastKommunikator("-=*Click*=-");

	}

	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlAndroiden) {
		int zufallsZ = (int) (Math.random() * 100);
		int reperaturAndroiden = 0;
		if (anzahlAndroiden > this.androidenAnzahl) {
			reperaturAndroiden = this.androidenAnzahl;
		} else {
			reperaturAndroiden = anzahlAndroiden;
		}
		int anzahlStrukturen = 0;
		if (schutzschilde) {
			anzahlStrukturen++;
		}
		if (energieversorgung) {
			anzahlStrukturen++;
		}
		if (schiffshuelle) {
			anzahlStrukturen++;
		}

		int reparaturProzente = (zufallsZ * reperaturAndroiden) / anzahlStrukturen;
		if (schutzschilde) {
			this.setSchildeInProzent(getSchildeInProzent() + reparaturProzente);

		}
		if (energieversorgung) {
			this.setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reparaturProzente);

		}
		if (schiffshuelle) {
			this.setHuelleInProzent(getHuelleInProzent() + reparaturProzente);
		}
	}

	public void zustandRaumschiff() {
		System.out.println("Photonentorpedo: " + this.photonentorpedoAnzahl);
		System.out.println("Energie:" + this.energieversorgungInProzent);
		System.out.println("Schilde: " + this.schildeInProzent);
		System.out.println("H�lle: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Schiffsname: " + this.schiffsname);
		System.out.println("Androiden:" + this.androidenAnzahl + "\n");
	}

	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}

	}

	public void ladungsverzeichnisAufraumen() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			RaumschiffLadung ladung = ladungsverzeichnis.get(i);
			if (ladung.getMenge() == 0) {
				ladungsverzeichnis.remove(i);
				i--;
			}
		}

	}

}
