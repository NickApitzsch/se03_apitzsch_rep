import java.util.Scanner;

public class Fahrkartenautomat {
	
	public static double fahrkartenbestellungErfassen() {
		System.out.println("Wie viele Fahrkarten wollen sie Kaufen?");
		Scanner myScanner = new Scanner (System.in);
		double double1 = myScanner.nextDouble();
		return double1;
	}
	public static double fahrkarteBezahlen(double zuZahlen) {
		System.out.println("Sie muessen " + zuZahlen + " Euro bezahlen" );
		Scanner myScanner = new Scanner (System.in);
		double double2 = myScanner.nextDouble();
		return double2 - zuZahlen;
	}
	
	public static boolean fahrkarteAusgeben(double rest) {
		if(rest==0) {
			return true;
		}
		else if(rest>0) {
			return rueckgeldAusgeben(rest);
		}
		else if (rest<0) {
			System.out.printf("Noch zu zahlender Betrag: %.2f Euro %n" , rest * -1);
			return false;
		}
		return false; }
		
	    public static boolean muenzeAusgeben(double rueckgeld) {
	    	rueckgeld = rueckgeld * 100;
	    	
	    	int zweiEuroCount = 0;
	    	int einEuroCount = 0;
	    	int fuenfizigCentCount = 0;
	    	int zwanzigCentCount = 0;
	    	int zenCentCount = 0;
	    	int fuenfCentCount = 0;
	    	int zweiCentCount = 0;
	    	int einCentCount = 0;
	    	
	        while (rueckgeld>=200){
	        	rueckgeld=rueckgeld-200;
	            zweiEuroCount = zweiEuroCount + 1;
	        }
	        while (rueckgeld>=100){
	        	rueckgeld=rueckgeld-100;
	        	einEuroCount = einEuroCount + 1;
	        }
	        while (rueckgeld>=50){
	        	rueckgeld=rueckgeld-50;
	        	fuenfizigCentCount = fuenfizigCentCount + 1;
	        }
	        while (rueckgeld>=20){
	        	rueckgeld=rueckgeld-20;
	        	zwanzigCentCount = zwanzigCentCount + 1;
	        }
	        while (rueckgeld>=10){
	        	rueckgeld=rueckgeld-10;
	        	zenCentCount = zenCentCount + 1;
	        }
	        while (rueckgeld>=5){
	        	rueckgeld=rueckgeld-5;
	        	fuenfCentCount = fuenfCentCount + 1;
	        }
	        while (rueckgeld>=2){
	        	rueckgeld=rueckgeld-2;
	        	zweiCentCount = zweiCentCount + 1;
	        }
	        while (rueckgeld>=1){
	        	rueckgeld=rueckgeld-1;
	        	einCentCount = einCentCount + 1;
	        }
	       
	        System.out.println(zweiEuroCount + " 2-Eurostuecke " + einEuroCount + " 1-Eurostuecke " + fuenfizigCentCount + " 50-Centstuecke " + zwanzigCentCount + " 20-Centstuecke " + zenCentCount + " 10-Centstuecke " + fuenfCentCount + " 5-Centstuecke " + zweiCentCount + " 2-Centstuecke " + einCentCount +  " 1-Centstuecke");
	        
	        return true;
	}
	public static boolean rueckgeldAusgeben(double rueckgeld) {
		System.out.printf("Ihr Rueckgeld betraegt: %.2f Euro %n", rueckgeld); 
		return true;
	}
	public static void warte(int millisekunde) throws InterruptedException {
        Thread.sleep(millisekunde);
    }


	public static void main(String[] args) throws InterruptedException {
		while (true) {
			final double preis = 6.99;
			double anzahlFahrkarten = fahrkartenbestellungErfassen();
			double zuZahlen = anzahlFahrkarten * preis;
			double rest = fahrkarteBezahlen(zuZahlen);
			
			boolean ret = fahrkarteAusgeben(rest);
			if(ret) {
				System.out.println("Ihre Fahrkarte wird gedruckt");
			}
			else {
				System.out.println("Ihre Fahrkarte wird nicht gedruckt");
			} 
			warte(10000);
		}
	}   

}